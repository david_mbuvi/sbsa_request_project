package main

import (
	"flag"
	"fmt"
	"medical_cli_app/api"
	"os"
)

func main() {

	// `RxNav get` subcommand
	getCmd := flag.NewFlagSet("get", flag.ExitOnError)

	//inputs for 'RxNav get' command
	getSearch := getCmd.String("search", "", "Get RxNorm name")
	getDescribe := getCmd.String("describe", "", " RxNav NDC10  code")

	if len(os.Args) < 1 {
		fmt.Println("expected `get`  subcommand")
		os.Exit(1)
	}
	//look at the 1 argument`s value
	switch os.Args[1] {
	case "get": // if its the `search` command
		api.HandleGet(getCmd, getSearch, getDescribe)
	default: //if we don`t understand the input
	}
}
