## MEDICAL CLI
This is a simplified command line golang app 
its intergrated with  National Library of Medicine apis
https://lhncbc.nlm.nih.gov/RxNav/
Its main purpose is to search RxNav medicine and provide details of those drugs:

It contains three functions at the moment
getCmd := flag.NewFlagSet("get", flag.ExitOnError)


	// `RxNav get` subcommand
	getCmd := flag.NewFlagSet("get", flag.ExitOnError)

	//inputs for 'RxNav get' command
	getSearch := getCmd.String("search", "", "Get RxNorm name")
	getDescribe := getCmd.String("describe", "", " RxNav NDC10  code")

To Execute it do the following:- 

Run app using
  
    go run cmd/server/main.go get -search "name you need to search"

if no data it will display as shown below
    
    go run cmd/server/main.go get  -search RxNorm

    https://rxnav.nlm.nih.gov/REST/rxcui.json?name=RxNorm&search=1
    0
    The name you are searching not found in our records

For help on how to use the app:- 
    
      go run cmd/server/main.go get -h
    Usage of get:
        -describe string
            RxNav NDC10  code
        -search string
            Get RxNorm name

To build binary file of the cli
 
    go build -o main .


using binary file :-
 
    ./main get -search "name"
for help 
  
    ./main get -h

The app is purely golang language