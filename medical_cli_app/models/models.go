package models

//A Responce struct to map the entire Response
type Response struct {
	NdcPropertyList NdcPropertyList `json:"ndcPropertyList"`
}

type NdcPropertyList struct {
	NdcProperty []NDCProperty `json:"ndcProperty"`
}
type NDCProperty struct {
	Ndc10               string              `json:"ndc10"`
	PropertyConceptList PropertyConceptList `json:"propertyConceptList"`
}
type PropertyConceptList struct {
	Concept []PropertyConcept `json:"propertyConcept"`
}

// A struct to map our PropertyConcept which includes it's propName and propValue
type PropertyConcept struct {
	PropName  string `json:"propName"`
	PropValue string `json:"propValue"`
}

//search response

type SearchResponse struct {
	IDGroup IDGroup `json:"idGroup"`
}
type IDGroup struct {
	Name     string   `json:"name"`
	RxnormID []string `json:"rxnormId"`
}
