package utils

import (
	"strconv"
	"strings"
)

func StrToIntArr(s string) []int {
	strs := strings.Split(s, " ")
	res := make([]int, len(strs))
	for i := range res {
		res[i], _ = strconv.Atoi(strs[i])
	}
	return res
}
