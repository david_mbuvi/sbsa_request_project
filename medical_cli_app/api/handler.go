package api

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"medical_cli_app/models"
	"net/http"
	"net/url"
	"os"
)

func HandleGet(getCmd *flag.FlagSet, search *string, describe *string) {
	getCmd.Parse(os.Args[2:])
	URL := "https://rxnav.nlm.nih.gov/REST/"
	if *search == "" && *describe == "" {
		fmt.Print("search_name is required or specify  -describe_name for NDC10 for all RxNav ")
		getCmd.PrintDefaults()
		os.Exit(1)
	}
	if *search != "" {
		name := *search
		param := url.Values{}
		param.Add("name", name)
		param.Add("search", "1")
		fmt.Println("\n", URL+"rxcui.json?"+param.Encode())
		responses, err := http.Get(URL + "rxcui.json?" + param.Encode())
		if err != nil {
			fmt.Print(err.Error())
			os.Exit(1)
		}
		responseSearchData, _ := ioutil.ReadAll(responses.Body)
		var responseObjects models.SearchResponse
		json.Unmarshal(responseSearchData, &responseObjects)

		fmt.Println(len(responseObjects.IDGroup.Name))
		if responseObjects.IDGroup.RxnormID == nil {
			fmt.Println("The name you are searching not found in our records \n", "You entered: \t", responseObjects.IDGroup.Name)
		} else {
			fmt.Println("Name: \t", responseObjects.IDGroup.Name)
			for _, s := range responseObjects.IDGroup.RxnormID {
				fmt.Println("RxnormID:\t", s)
			}
		}

	}
	if *describe != "" {
		id := *describe
		params := url.Values{}
		params.Add("id", id)
		response, err := http.Get(URL + "ndcproperties.json?" + params.Encode())
		if err != nil {
			fmt.Print(err.Error())
			os.Exit(1)
		}
		responseData, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err)
		}

		var responseObject models.Response
		json.Unmarshal(responseData, &responseObject)

		for i := 0; i < len(responseObject.NdcPropertyList.NdcProperty); i++ {
			fmt.Println("NDC10", responseObject.NdcPropertyList.NdcProperty[i].Ndc10)
			for _, s := range responseObject.NdcPropertyList.NdcProperty[i].PropertyConceptList.Concept {
				fmt.Println(s.PropName, "\t", s.PropValue)
			}
		}
	}
}
