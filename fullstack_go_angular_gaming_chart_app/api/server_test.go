package api

import (
	"fullstack_go_angular_gaming_chart_app/util"
	"github.com/stretchr/testify/require"
	"testing"
)

func NewTestServer(t *testing.T) *Server {
	config := util.Config{
		ServerAddress: util.GenIpaddress(),
	}

	server, err := NewServer(config)
	require.NoError(t, err)

	return server
}
