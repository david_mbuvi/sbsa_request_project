package api

import (
	"fullstack_go_angular_gaming_chart_app/util"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
)

// Server serves HTTP requests for our Gaming-chart service

type Server struct {
	config util.Config
	router *gin.Engine
}

//NewServer creates a new HTTP server and set up routing

func NewServer(config util.Config) (*Server, error) {
	//initialize file opening and closing

	server := &Server{
		config: config,
	}
	server.SetupRouter()
	return server, nil
}

//setuprouters for all routes

func (server *Server) SetupRouter() {
	router := gin.Default()
	router.Use(cors.Default())
	// router.Use(gin.Logger())

	// this is the default route for the front end.  The angular app will handle
	// all non-api calls
	router.Use(static.Serve("/", static.LocalFile("./client", false)))

	//Routing group
	//Extract the common prefix and create a routing group
	api := router.Group("/")
	{
		//allgames
		api.GET("/all_games", server.all_games_list)
		// sort games based on playtime
		api.GET("/select_top_by_playtime", server.select_top_by_playtime)

		// sort games based on unique userId  from highiest
		api.GET("/select_top_by_players", server.select_top_by_players)
	}
	router.NoRoute(func(c *gin.Context) {
		c.File("./client/index.html")
	})
	server.router = router
}

// Start run the HTTP server on a specific address.
func (server *Server) Start(address string) error {
	return server.router.Run(address)
}
