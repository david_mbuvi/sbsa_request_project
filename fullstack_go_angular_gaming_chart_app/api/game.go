package api

import (
	"fullstack_go_angular_gaming_chart_app/model"
	"github.com/gin-gonic/gin"
	"net/http"
)

//game response
type GameResponse struct {
	UserId          int64    `json:"userid"`
	GameType        string   `json:"game"`
	Genre           string   `json:"genre"`
	Platforms       []string `json:"platforms"`
	TotalPlayTime   int64    `json:"totalPlayTime"`
	NumberOfPlayers int      `json:"totalPlayers"`
}

//all games function

func (server *Server) all_games_list(context *gin.Context) {
	//game model
	var game []model.Game
	// All Games Dataset
	gameDataSet := model.GetAllGames()
	//loop into the game dataset and update the struct game models.
	for i := 0; i < len(gameDataSet.Games); i++ {
		if gameDataSet.Games != nil {
			game = append(game, gameDataSet.Games[i])
		}
	}
	//response
	context.JSON(http.StatusOK, game)
}

// filterGamesByOptions Returns Filtered games based on genre and platform
func filterGamesByOptions(context *gin.Context) (filteredGames []model.Game) {
	// Get genre from request
	genre := context.Query("genre")
	if genre == "" {
		context.JSON(http.StatusBadRequest,
			gin.H{
				"code":    http.StatusBadRequest,
				"message": string("Genre is required for filtering"),
			})
		return
	}
	platform := context.Query("platform")
	if platform == "" {
		context.JSON(http.StatusBadRequest,
			gin.H{
				"code":    http.StatusBadRequest,
				"message": string("Platforms is required for filtering"),
			})
		return
	}

	// All Games Dataset
	gameDataSet := model.GetAllGames()

	// check platform
	for i := 0; i < len(gameDataSet.Games); i++ {
		if gameDataSet.Games[i].Platforms != nil {
			for _, s := range gameDataSet.Games[i].Platforms {
				if s == platform && gameDataSet.Games[i].Genre == genre {
					filteredGames = append(filteredGames, gameDataSet.Games[i])
				}
			}
		}
	}
	sortedFiltedGamesData := model.ByUserId(filteredGames)
	return sortedFiltedGamesData
}

// Write a function named select_top_by_playtime. select_top_by_playtime is used to return the most played games from this set.
//The most played games are the games that have the highest total playtime between users.
//It has the interface select_top_by_playtime(dataSet [, options]). The options available should include:
//- `genre`: return only the games that are classified under this genre.
//- `platform`: return only the games that available under this platform.

func (server *Server) select_top_by_playtime(context *gin.Context) {
	/**todos
	  // check if game type exist, if it exists
	  //check if userId exist with the same game
	  //total time == current time + newtime
	  **/
	filteredGames := filterGamesByOptions(context)
	//var topPlayers []TopPlayedGamesResponse
	highestTotalPlaytime := make(map[string]*GameResponse)
	// Loop through filtered Game data set - range
	for _, filteredGame := range filteredGames {
		// check if gameType already exists in topPlayers. check if not exist append
		game, ok := highestTotalPlaytime[filteredGame.GameType]
		if !ok {
			//update the struct
			game = &GameResponse{
				UserId:        filteredGame.UserId,
				GameType:      filteredGame.GameType,
				Genre:         filteredGame.Genre,
				Platforms:     filteredGame.Platforms,
				TotalPlayTime: filteredGame.PlayTime,
			}
			highestTotalPlaytime[filteredGame.GameType] = game
		} else if filteredGame.GameType != game.GameType {
			game.TotalPlayTime = filteredGame.PlayTime

		} else {
			game.TotalPlayTime = game.TotalPlayTime + filteredGame.PlayTime

		}

	}
	// converting topPlayer map data type into json.
	playedTimeData := DataSliceToJsonFormart(highestTotalPlaytime)

	// Respond with filtered playedTimeData
	context.JSON(http.StatusOK, playedTimeData)
}

//Write a function named select_top_by_players. select_top_by_players is used to return the games with the highest number of unique players (determined by their userId).
//It has the interface select_top_by_players(dataSet [, options]). The options available should include:
//- `genre`: return only the games that are classified under this genre.
//- `platform`: return only the games that are available under this platform.
// Game Response

func (server *Server) select_top_by_players(context *gin.Context) {
	filteredGames := filterGamesByOptions(context)
	//var topPlayers []TopPlayedGamesResponse
	topPlayers := make(map[string]*GameResponse)
	// Loop through filtered Game data set - range
	for _, filteredGame := range filteredGames {
		// check if gameType already exists in topPlayers. check if not exist append
		game, ok := topPlayers[filteredGame.GameType]
		if !ok {
			//update the struct
			game = &GameResponse{
				UserId:          filteredGame.UserId,
				GameType:        filteredGame.GameType,
				Genre:           filteredGame.Genre,
				Platforms:       filteredGame.Platforms,
				NumberOfPlayers: 1,
			}
			topPlayers[filteredGame.GameType] = game
			// check if game type exist, if it exists
			//check if userId exist with the same game
			//Increment NumberOfPlayers by 1
		} else if filteredGame.GameType == game.GameType {
			if filteredGame.UserId != game.UserId {
				game.NumberOfPlayers = game.NumberOfPlayers + 1
			}
		}

	}
	// converting topPlayer map data type into json.
	playerData := DataSliceToJsonFormart(topPlayers)
	context.JSON(http.StatusOK, playerData)
}

//sorting based on highly played
func DataSliceToJsonFormart(m map[string]*GameResponse) []*GameResponse {
	games := make([]*GameResponse, 0, len(m))
	for k, v := range m {
		v.GameType = k
		games = append(games, v)
	}
	return games
}
