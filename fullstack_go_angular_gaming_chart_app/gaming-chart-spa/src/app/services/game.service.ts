import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {Games} from "../model/games";
import {catchError, map, tap} from "rxjs/operators";

const baseUrl = 'http://localhost:8080';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private http: HttpClient) {
  }

  /**Gets all Games**/
  gamesList(): Observable<Games[]> {
    return this.http.get<any>(baseUrl + '/all_games').pipe(map(res => res as Games[]));
  }

  /**Gets all select_top_by_playtime **/

  getSelectedGamesByTime(genre: string, platform: string): Observable<Games[]> {

    let params = new HttpParams()
      .set('genre', genre)
      .set('platform', platform);

    return this.http.get<Games[]>(baseUrl + '/select_top_by_playtime', {params}).pipe(
      tap(x => x.length ?
        console.log(`found Games matching "${params}"`) :
        console.log(`no Games matching "${params}"`)),
      catchError(this.handleError<Games[]>('getSelectedGamesByTime', [])
      )
    );
  }

  /**Gets all select_top_by_players **/
  getSelectedGamesByPlayer(genre: string, platform: string): Observable<Games[]> {

    let params = new HttpParams()
      .set('genre', genre)
      .set('platform', platform);

    return this.http.get<Games[]>(baseUrl + '/select_top_by_players', {params}).pipe(
      tap(x => x.length ?
        console.log(`found Game matching "${params}"`) :
        console.log(`no game matching "${params}"`)),
      catchError(this.handleError<Games[]>('getSelectedGamesByPlayer', [])
      )
    );
  }

  /** Handle error **/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
