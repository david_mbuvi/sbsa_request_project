import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {PlaytimeComponent} from "./components/playtime/playtime.component";
import {PlayerComponent} from "./components/player/player.component";

const routes: Routes = [
  {path: '', component: DashboardComponent},
  {path: 'select_top_by_playtime', component: PlaytimeComponent},
  {path: 'select_top_by_players', component: PlayerComponent},
  {path: '', redirectTo: '/', pathMatch: 'full'}, // redirect to `first-component`
  // { path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
