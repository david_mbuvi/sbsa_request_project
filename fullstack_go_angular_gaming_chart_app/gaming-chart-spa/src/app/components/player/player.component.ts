import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Games} from "../../model/games";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpParams} from "@angular/common/http";
import {GameService} from "../../services/game.service";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogConfig} from "@angular/material/dialog";

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
  searchForm = new FormGroup({
    genre: new FormControl('', [Validators.required, Validators.minLength(2)]),
    platform: new FormControl('', [Validators.required, Validators.minLength(2)])
  });

  responseMessage = '';

  submitted = false; // show and hide the success message
  isLoading = false; // disable the submit button if we're loading
  dataSource = new MatTableDataSource<any>();
  games: Games[] = [];
  displayedColumns: string[] = ['game', 'platforms', 'genre', 'totalPlayers'];
  @ViewChild(MatSort, {static: true}) sort: MatSort | any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator | any;

  constructor(private formBuilder: FormBuilder, public dialog: MatDialog, private http: HttpClient, private gameService: GameService) {
    this.dataSource = new MatTableDataSource(this.games)
    this.isLoading = true;
  }

  ngOnInit() {
    this.loadGames()
  }

  /** LoadGames function implementation retriving allgames from the backend**/
  loadGames() {
    this.gameService
      .gamesList()
      .pipe(
        catchError(() => of([])),
        finalize(() => this.isLoading = false)
      ).subscribe(
      games => {
        this.games = games;
        this.dataSource.data = this.games;
        this.isLoading = false;
      }
    );
  }

  /** ngAfterViewInit function implementation to sort games after retrival from the backend**/
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  /** Filter function implementation on each column**/
  applyFilter() {
    const filterValue = (event?.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  /** search function implementation to view a single game**/
  getSelectedFilter() {
    const genre = this.searchForm.get("genre")?.value
    const platform = this.searchForm.get("platform")?.value
    this.isLoading = true;
    this.responseMessage = '';
    this.gameService.getSelectedGamesByPlayer(genre, platform)
      .subscribe((games) => {
          this.games = games;
          this.dataSource.data = this.games;
        },
        (error) => {
          this.responseMessage = error.message;
          this.isLoading = false;
        },
        () => {
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.isLoading = false;
        })
  }

  /** DialogComponent to view a single game**/
  onRowClicked(row: any) {
    //close on click outside
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose=true;
    dialogConfig.autoFocus=true;
    dialogConfig.width="60%";
    // @ts-ignore
    const dialogRef = this.dialog.open(PlayerDialog, dialogConfig,{
      height: '350px',
      width: '350px',
      data: {row}
    });

    dialogRef.afterClosed();
  }
}

/**
 *Player sorting dialog for individual games
 * **/
@Component({
  selector: 'player-dialog',
  template: `
    <button style="padding: 5px;line-height: 14px;
  min-width: auto;" mat-raised-button class="close-icon" (click)="onNoClick()">
      <mat-icon>close</mat-icon>
    </button>
    <h1 mat-dialog-title>{{game}}</h1>
    <div mat-dialog-content style="display: block; text-indent: each-line">
      <p><strong>Genre</strong>: {{genre}}</p>
      <p><strong>Platforms</strong>: {{platforms}}</p>
      <p><strong>Number of Players</strong>: {{totalPlayers}}</p>
    </div>
    <mat-divider></mat-divider>
  `
})
export class PlayerDialog {
  /**
   * Game interface initilization
   * **/
  game?: string
  genre?: string
  platforms?: string[]
  totalPlayers?: number

  constructor(
    public dialogRef: MatDialogRef<PlayerDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.game = this.data.row.game;
    this.genre = this.data.row.genre;
    this.platforms = this.data.row.platforms;
    this.totalPlayers = this.data.row.totalPlayers;
  }

  /** Onclick function  to close single game view dialog**/
  onNoClick() {
    this.dialogRef.close();
  }
}
