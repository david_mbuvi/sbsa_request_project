import {Component, HostListener, Inject, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {GameService} from "../../services/game.service";
import {of, Subscription} from "rxjs";
import {catchError, finalize} from "rxjs/operators";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";

interface Game {
  userId?: number
  game?: string
  genre?: string
  platforms?: string[]
  playTime?: number
}


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  displayedColumns: string[] = ['userId', 'game', 'genre', 'platforms', 'playTime'];
  dataSource = new MatTableDataSource<any>();
  games: Game[] = [];
  isLoading = true;
  @ViewChild(MatSort, {static: true}) sort: MatSort | any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator | any;

  constructor(public dialog: MatDialog, private gameService: GameService) {
    this.dataSource = new MatTableDataSource(this.games)
  }

  ngOnInit() {
    this.loadGames()
  }

  loadGames() {
    this.gameService
      .gamesList()
      .pipe(
        catchError(() => of([])),
        finalize(() => this.isLoading = false)
      ).subscribe(
      games => {
        this.games = games;
        this.dataSource.data = this.games;
        this.isLoading = false;
      }
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter() {
    const filterValue = (event?.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  /** DialogComponent to view a single game**/
  onRowClicked(row: any) {
    const dialogRef = this.dialog.open(DashboardDialog, {
      height: '350px',
      width: '350px',
      data: {row}
    });

    dialogRef.afterClosed();
  }
}


@Component({
  selector: 'dashboard-dialog',
  template: `
    <button style="padding: 5px;line-height: 14px;
  min-width: auto;" mat-raised-button class="close-icon" (click)="onNoClick()">
      <mat-icon>close</mat-icon>
    </button>
    <h1 mat-dialog-title>{{game}}</h1>
    <div mat-dialog-content style="display: block; text-indent: each-line">
      <p><strong>UserId</strong>: {{userId}}</p>
      <p><strong>Genre</strong>: {{genre}}</p>
      <p><strong>Platforms</strong>: {{platforms}}</p>
      <p><strong>PlayTime</strong>: {{playTime}}</p>
    </div>
    <mat-divider></mat-divider>
  `
})
export class DashboardDialog {
  userId?: number
  game?: string
  genre?: string
  platforms?: string[]
  playTime?: number

  constructor(
    public dialogRef: MatDialogRef<DashboardDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.userId = this.data.row.userId;
    this.game = this.data.row.game;
    this.genre = this.data.row.genre;
    this.platforms = this.data.row.platforms;
    this.playTime = this.data.row.playTime;
  }

  /** Onclick function  to close single game view dialog**/

  onNoClick(): void {
    this.dialogRef.close();
  }
}
