export interface Games {
  game: string
  genre: string
  platforms: string[]
  totalPlayTime: number
  totalPlayers: number
}
