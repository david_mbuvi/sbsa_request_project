import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'gaming-chart-spa';
  sideBarOpen = true;

  constructor() {
  }

  ngOnInit(): void {
  }

  sideBarToggle() {
    this.sideBarOpen = !this.sideBarOpen;
  }
}
