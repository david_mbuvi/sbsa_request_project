package main

import (
  "github.com/gin-gonic/gin"
  "os"
  "testing"
)

//main entry test
func TestMain(m *testing.M) {
  gin.SetMode(gin.TestMode)

  os.Exit(m.Run())
}


