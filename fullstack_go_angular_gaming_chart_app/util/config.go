package util

import (
  "github.com/spf13/viper"
)

// Config stores all configuration of the application.
// The values are read by viper from a config file or environment variable.

type Config struct {
  ServerAddress string `mapstructure:"SERVER_ADDRESS"`
}

// LoadConfig reads configuration from file or environment variables.
func LoadConfig(path string) (config Config, err error) {
  viper.AddConfigPath(path)
  viper.SetConfigName("app")
  viper.SetConfigType("env")

  // it automatically checks if environment has changed then it syncs tha app to that environment
  viper.AutomaticEnv()

  err = viper.ReadInConfig()
  if err != nil {
    return
  }
  // I unmarshal the Config which contains
  // Configaration  content into config which I defined above
  err = viper.Unmarshal(&config)
  return
}
