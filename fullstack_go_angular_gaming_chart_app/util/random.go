package util

import (
  "fmt"
  "math/rand"
  "strings"
  "time"
)

const alphabet = "abcdefghijklmnopqrstuvwxyz"

func init() {
  rand.Seed(time.Now().UnixNano())
}

// RandomInt generates a random integer between min and max
func RandomInt(min, max int64) int64 {
  return min + rand.Int63n(max-min+1)
}

// RandomString generates a random string of length n
func RandomString(n int) string {
  var sb strings.Builder
  k := len(alphabet)

  for i := 0; i < n; i++ {
    c := alphabet[rand.Intn(k)]
    sb.WriteByte(c)
  }

  return sb.String()
}

// RandomGameType generates a random game name
func RandomGameType() string {
  return RandomString(6)
}

// RandomUserId generates a random UserId
func RandomUserId() int64 {
  return RandomInt(0, 100)
}

// RandomGenre/ platform generates a random platform/genre
func RandomGenre() string {
 var  PC,FPS, RPG,PS4 = "","","",""
  genres := []string{PC, FPS, RPG,PS4}
  n := len(genres)
  return genres[rand.Intn(n)]
}

func GenIpaddress() string {
  rand.Seed(time.Now().Unix())
  ip := fmt.Sprintf("%d.%d.%d.%d", rand.Intn(255), rand.Intn(255), rand.Intn(255), rand.Intn(255))
  return ip
}
