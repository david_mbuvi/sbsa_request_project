# Backend technology used
Golang language
- gin framework
- viper

# Backend structure

- api :- Contains Server and Game file. server.go contains all api routing and initilization of 
app  while game.go holds all function of app.
- client :- it a folder containing final output of SPA.
- data :- it contains the games.json file.
- gaming-chart-spa:- it is the actual frontend files.
- model :- it contains the game struct and Fetching data function.
- Output_image:- it contains images for final output.
- util :- it contains config file for viper to load serveraddress and  random.go to generate required testend functions.
- app.env:- it contains the serverIPAddress
- gaming_chart :- its the build go app in binary files ready for to be deployed for production.
- go.mod :- it contains all dependency loaded into the app.
- main.go :- it the entry point of the app.
-makefile :- it contains shorthand command you can use to run the application.
  - README.md :- It contains project description.

#Install app dependency

- Initialize app by running the following command

**go mod init gaming-chart**

- To download app dependency
  
 - viper for app environment management

**go get github.com/spf13/viper**
  
- Gin framework for entire app development

  **go get -u github.com/gin-gonic/gin**

#Steps to run app using makefile
To run app in development environment.
 - **make server** :- it executes the go run main.go 
 - **make build_server** :- it builds app for production and run it
 - **make test** :- to run all test files within the app.
# You can view the Gaming_chart on your IPAddress or localhost as follows
#### http://127.0.0.1:8080

-------------------
# SPA description
 I have used javascript/typescript language for Angular Framework.

# Gaming-chart-spa installation 
I used the command below to initiate app folder

- **ng new gaming-chart-spa**

#SPA structure 

- package.json :- contains all dependency for the app.
- src- app :- contains all component,model,service,shared components.
     - components :-  contains the dashboard,player and playtime components.
     - model :- it contains game interface.
     - service :- it contains all game service for interacting with the server backend.
     - shared :- it contains the header,sidebar and footer components.
  
- app-routing.module.ts :- contains routers for the entire app.
- app.module.ts :- contains all modules required by app.


# Dependency
used the following dependency:-
- angular material
- flex-layout
  
#### Installation of the dependency
- **npm  install  @@angular/flex-layout**
- **ng add @angular-material**

#Steps to run SPA using the makefile

- **make client_spa**:- it accesses into gaming-chart-spa folder and running the spa files using ng serve --open to launch on your default browser
- **make build_client** :- it accesses the gaming-chart-spa and build the client folder for production

You can view the Gaming-chart-spa on your IPAddress or localhost as follows
http://127.0.0.1:4200

--------------

#FINAL OUTPUT IMAGES

# All Games
![All_games](./Output_images/All_Game.png)
# Top Games By PlayTime
![All_games](./Output_images/Top_Game_sorted_by_Playtime.png)


#Top Games  By PlayTime
![All_games](./Output_images/Top_Games_by_number_of_player.png)

#Single Game View
![All_games](./Output_images/Single_game_view.png)


------------------

## Time Spend


Day                |Task Accomplished                        |Time taken |
|----------------|-------------------------------|-----------------------------|
| `25/06/2021` | implementation of Backend and SPA environment | 30min |
| `28/06/2021`  | Model for game structure implementation| 1hrs |
| `29/06/2021` | Api implementation and test | 2hrs |
| `30/06/2021` | Frontend Design and integration with backend  | 2hrs |
| `01/07/2021` |  Finalizing Frontend end points | 2hrs |
| `02/07/2021` | Testing All Functions | 2hrs |


live link [gaming_chart](https://gaming-chart.herokuapp.com)