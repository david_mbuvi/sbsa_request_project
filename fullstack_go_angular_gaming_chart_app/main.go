package main

import (
	"fmt"
	"fullstack_go_angular_gaming_chart_app/api"
	"fullstack_go_angular_gaming_chart_app/util"
	"log"
)

func main() {

	/*  b := model.ShowAllGames()
	    fmt.Println(b,"\n")*/

	//load configuration from util folder config file
	config, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}
	//calling server
	server, err := api.NewServer(config)
	if err != nil {
		log.Fatal("cannot create server", err)
	}

	err = server.Start(config.ServerAddress)
	if err != nil {
		log.Fatal("Cannot start server:", err)
	} else {
		fmt.Println("welcome to Gaming Chart-Junior Server connected")
	}

}
