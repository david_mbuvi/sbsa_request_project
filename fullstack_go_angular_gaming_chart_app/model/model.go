package model

import (
  "encoding/json"
  "io/ioutil"
  "os"
)

// Game struct which contains all game  details
// a type and a list of platforms
type Game struct {
	UserId    int64    `json:"userId"`
	GameType  string   `json:"game"`
	PlayTime  int64    `json:"playTime"`
	Genre     string   `json:"genre"`
	Platforms []string `json:"platforms"`
}

// AllGames struct which contains
// an array of games
type AllGames struct {
	Games []Game `json:"data"`
}

//error handles
func Check(err error) {
	if err != nil {
		panic(err)
	}
}

//show all games in the file
func GetAllGames() (gs AllGames) {
	// Open  gamesJson file
	gamesJson, err := os.Open("./data/games.json")
	// if  os.Open returns an error then handle it
	Check(err)

	// defer the closing of  gamesJson file so that I can parse it later on
	defer gamesJson.Close()

	// read the opened gamesJson as a byte array.
	byteValue, _ := ioutil.ReadAll(gamesJson)

	// I initialize  allGames array
	var allGames AllGames

	// I unmarshal the byteArray which contains
	// gamesJson's content into 'allGames' which I defined above
	_ = json.Unmarshal(byteValue, &allGames)
	Check(err)

	return allGames
}

//sorting
type ByUserId []Game

func (t ByUserId) Len() int {
	return len(t)
}

func (t ByUserId) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func (t ByUserId) Less(i, j int) bool {
	return t[i].UserId > t[j].UserId
}
