package model

import (
  "testing"
)

//test reading from a file
func TestReadJSON(t *testing.T) {
  games := GetAllGames()
  //testing sample
  // "userId": 99,
  //      "game": "Minecraft",
  //      "playTime": 1002,
  //      "genre": "Sandbox",
  //      "platforms": ["PC"]
  expectedGame := "Minecraft"
  expectedPlayTime  := int64(1002)
  expectedGenre := "Sandbox"
  for _, s := range games.Games {
    if s.UserId == 99 {
      if expectedGame != s.GameType {
        t.Errorf("s.GameType == %q, want %q",
          s.GameType, expectedGame)
      }
      if expectedPlayTime != s.PlayTime {
        t.Errorf("s.PlayTime == %q, want %q",
          s.PlayTime, expectedPlayTime)
      }
      if expectedGenre != s.Genre {
        t.Errorf("s.Genre == %q, want %q",
          s.Genre, expectedGenre)
      }

    }

  }


}
