provider "aws" {
  region = "us-east-2"
}

variable "app_name" {
  description = "Application name"
  default     = "bithday_wish_message"
}

variable "app_env" {
  description = "Application environment tag"
  default     = "dev"
}

locals {
  app_id = "${lower(var.app_name)}-${lower(var.app_env)}-${random_id.unique_suffix.hex}"
}

data "archive_file" "lambda_zip" {
  type        = "zip"
  source_file = "../build/main" ## points to the main.go file
  output_path = "build/main.zip" ## sored within the terraform file
}

resource "random_id" "unique_suffix" {
  byte_length = 2
}

output "api_url" {
  value = aws_api_gateway_deployment.api_deployment.invoke_url
}