Hello!

This directory contains a few of projects

Projects stack covered here:-
   - Postgres DB
   - AWS ecosystems such as lambda,AWS Dynamic DB,AWS S3,AWS EC2 ,AWS Elastic Bean --> for a fullstack app
   - Golang main language
   - Angular frontend
   - Terraform
   - Gitlab
   - Docker
   - K8s and K9s

For sample of gitlab-Ci-Cd :-
kindly refer to this project link
[angular_ecommerce app ]( https://gitlab.com/david_mbuvi/e_app.git )

link to the app hosted := [Hosted_ecommerce](https://pacific-island-98494.herokuapp.com)

I have provided a sample of terraform files I have been using to deploy app to AWS
and for demo on integration of terraform,gitlab and AWS resource, i can provide on request.
