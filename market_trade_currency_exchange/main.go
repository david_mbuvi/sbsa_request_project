package main

import (
	"database/sql"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	"log"
	"market_trade_currency_exchange/api"
	db "market_trade_currency_exchange/db/sqlc"
	"market_trade_currency_exchange/util"
)

func main() {
	config, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	conn, err := sql.Open(config.DBDriver, config.DBSource)
	if err != nil {
		log.Fatal("cannot connect to db:", err)
	}
	runDBMigration(config.MigrationURL, config.DBSource)
	store := db.NewStore(conn)
	server, err := api.NewServer(config, store)
	if err != nil {
		log.Fatal("cannot create server:", err)
	}
	data := server.LoadData()
	for _, value := range data.Rate {
		if value.Date != "" {
			for _, element := range value.Cube {
				var exists bool
				row := conn.QueryRow("SELECT EXISTS (SELECT * FROM rates where date = $1  AND currency = $2 AND rate = $3 )", value.Date, element.Currency, element.Rate)
				if err := row.Scan(&exists); err != nil {
					log.Println(err)
				} else if !exists {
					if err, _ := conn.Exec("INSERT INTO rates (date, currency, rate) VALUES ($1, $2, $3)", value.Date, element.Currency, element.Rate); err != nil {
						log.Println(`Date Already created`, err)
					}
				}
			}
		}
	}
	log.Println("loaded successfully")

	err = server.Start(config.HTTPServerAddress)
	if err != nil {
		log.Fatal("cannot start server:", err)
	}
}

func runDBMigration(migrationURL string, dbSource string) {
	migration, err := migrate.New(migrationURL, dbSource)
	if err != nil {
		log.Fatal("cannot create new migrate instance:", err)
	}

	if err = migration.Up(); err != nil && err != migrate.ErrNoChange {
		log.Fatal("failed to run migrate up:", err)
	}

	log.Println("db migrated successfully")
}
