-- name: CreateRate :one
INSERT INTO rates (date,currency, rate) VALUES ($1, $2, $3) RETURNING *;

-- name: ListRate :many
SELECT m.currency,m.rate FROM rates m
INNER JOIN (select MAX(date) AS MaxDate from rates) r ON m.date = r.MaxDate
GROUP BY 1,2
ORDER BY 1,2;


-- name: ListRateOnDate :many
SELECT currency,rate FROM rates
WHERE date ilike $1
ORDER BY currency;

-- name: ListRateAnalysis :many
SELECT currency, CAST (MIN(rate) as float ) as min, CAST (MAX(rate)as float )as max,CAST (AVG(rate) as float) as avg FROM rates
GROUP BY currency;