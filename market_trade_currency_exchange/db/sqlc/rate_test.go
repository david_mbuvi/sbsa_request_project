package db

import (
	"context"
	"github.com/stretchr/testify/require"
	"market_trade_currency_exchange/util"
	"testing"
)

const date = "2021-04-05"

func createRandomRate(t *testing.T) Rate {
	arg := CreateRateParams{
		Date:     date,
		Currency: util.RandomCurrency(),
		Rate:     util.RandomRate(1.4342, 100.1234),
	}
	rate, err := testQueries.CreateRate(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, rate)

	require.Equal(t, arg.Date, rate.Date)
	require.Equal(t, arg.Currency, rate.Currency)
	require.Equal(t, arg.Rate, rate.Rate)
	require.NotZero(t, rate.CreatedAt)
	return rate

}
func TestCreateRate(t *testing.T) {

	for i := 0; i < 5; i++ {
		createRandomRate(t)
	}
}
func TestListRateOnDate(t *testing.T) {
	arg := date
	rates, err := testQueries.ListRateOnDate(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, rates)

	for _, value := range rates {
		require.NotEmpty(t, value)
	}
}

func TestListRateAnalysis(t *testing.T) {
	rates, err := testQueries.ListRateAnalysis(context.Background())
	require.NoError(t, err)
	require.NotEmpty(t, rates)

	for _, value := range rates {
		require.NotEmpty(t, value)
	}

}
