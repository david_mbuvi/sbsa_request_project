CREATE TABLE "rates" (
                           "id" bigserial PRIMARY KEY,
                           "date" varchar NOT NULL,
                           "currency" varchar NOT NULL,
                           "rate" float  NOT NULL,
                           "created_at" timestamptz NOT NULL DEFAULT (now())
);

CREATE INDEX ON "rates" ("id");

CREATE INDEX ON "rates" ("date");

CREATE INDEX ON "rates" ("currency");

COMMENT ON COLUMN "rates"."rate" IS 'must be positive';