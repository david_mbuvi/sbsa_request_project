package util

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// RandomFloat generates a random integer between min and max
func RandomRate(min, max float64) float64 {
	return (rand.Float64() * (max - min)) + min
}

// RandomCurrency generates a random currency code
func RandomCurrency() string {
	currencies := []string{"AUD", "BGN", "BRL", "CAD", "CHF", "CNY", "CZK", "DKK", "GBP", "HKD", "HRK", "HUF",
		"IDR", "ILS", "INR", "ISK", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PLN",
		"RON", "SEK", "SGD", "THB", "TRY", "USD", "ZAR"}
	n := len(currencies)
	return currencies[rand.Intn(n)]
}

// RandomInt generates a random integer between min and max
func RandomInt(min, max int64) int64 {
	return min + rand.Int63n(max-min+1)
}
