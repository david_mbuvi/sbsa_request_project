package api

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type Cube struct {
	Date string `xml:"time,attr"`
	Cube []struct {
		Currency string  `xml:"currency,attr"`
		Rate     float64 `xml:"rate,attr"`
	}
}

type Gesmes struct {
	Rate RateList `xml:"Cube>Cube"`
}

type RateList []Cube

// read data from the url and add to the database
func (server *Server) LoadData() *Gesmes {
	url := "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"
	method := "GET"
	client := &http.Client{}
	client.Timeout = time.Second*60 + 20
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	var results = Gesmes{}
	err = xml.Unmarshal(data, &results)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Loading data to database begun")

	return &results
}
