package api

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"net/http"
)

const base = "EUR"

type RateResponse struct {
	Base  string             `json:"base"`
	Rates map[string]float64 `json:"rates"`
}
type Response struct {
	Currency string  `json:"currency"`
	Rate     float64 `json:"rate"`
}

//Develop an endpoint that will return the latest exchange rate. Currency should be sorted in ascending order.
func (server *Server) listlatestExchangeRate(ctx *gin.Context) {
	resp, err := server.store.ListRate(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	responseRate := make(map[string]float64)
	for _, value := range resp {
		responseRate[value.Currency] = value.Rate
	}
	rates := RateResponse{
		Base:  base,
		Rates: responseRate,
	}

	ctx.JSON(http.StatusOK, rates)
}

func Results(arr []Response) (rates RateResponse) {
	s := make(map[string]float64)
	for _, value := range arr {
		s[value.Currency] = value.Rate
	}
	rates = RateResponse{
		Base:  base,
		Rates: s,
	}
	return rates
}

type listRateRequest struct {
	Date string `json:"date" `
}

//Develop an endpoint that will return the exchange rate on specific date. Currency should be sorted in ascending order. Response format is the same as task 2.
func (server *Server) getExchangeRateOnSpecificDate(ctx *gin.Context) {
	var req listRateRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	rate, err := server.store.ListRateOnDate(ctx, req.Date)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	responseRate := make(map[string]float64)
	for _, value := range rate {
		responseRate[value.Currency] = value.Rate
	}
	rates := RateResponse{
		Base:  base,
		Rates: responseRate,
	}
	ctx.JSON(http.StatusOK, rates)
}

type listRateAnalysesResponse struct {
	Base          string                 `json:"base"`
	Rates_analyze map[string]interface{} `json:"rates_Analyze"`
}

//Develop an endpoint that will return the minimum, maximum and average exchange rates based on the data loaded in task 1.
func (server *Server) listExchangeRateAnalyses(ctx *gin.Context) {
	resp, err := server.store.ListRateAnalysis(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	responseRate := make(map[string]interface{})
	for _, value := range resp {
		responseRate[value.Currency] = map[string]interface{}{
			"min": value.Min,
			"max": value.Max,
			"avg": value.Avg,
		}
	}

	response := listRateAnalysesResponse{
		Base:          base,
		Rates_analyze: responseRate,
	}

	ctx.JSON(http.StatusOK, response)
}
