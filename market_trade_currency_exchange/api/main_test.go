package api

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"
	db "market_trade_currency_exchange/db/sqlc"
	"market_trade_currency_exchange/util"
	"os"
	"testing"
)

func newTestServer(t *testing.T, store db.Store) *Server {
	config := util.Config{}
	server, err := NewServer(config, store)
	require.NoError(t, err)

	return server
}
func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)
	os.Exit(m.Run())
}
