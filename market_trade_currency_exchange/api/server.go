package api

import (
	"github.com/gin-gonic/gin"
	"io"
	db "market_trade_currency_exchange/db/sqlc"
	"market_trade_currency_exchange/util"
	"os"
)

// Server serves HTTP requests for rakuten_api service

type Server struct {
	config util.Config
	store  db.Store
	router *gin.Engine
}

// NewServer creates anew HTTP server and set up routing.
func NewServer(config util.Config, store db.Store) (*Server, error) {
	server := &Server{
		config: config,
		store:  store,
	}
	server.setupRouter()
	return server, nil

}

func (server *Server) setupRouter() {
	//Disable Console Color.
	gin.DisableConsoleColor()
	f, _ := os.Create("./app_logs/gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	router := gin.Default()

	router.GET("/rates/latest", server.listlatestExchangeRate)
	router.GET("/rates/YYY-MM-DD", server.getExchangeRateOnSpecificDate)
	router.GET("/rates/analyze", server.listExchangeRateAnalyses)

	server.router = router
}

// Start runs the HTTP server on a specific address.
func (server *Server) Start(address string) error {
	return server.router.Run(address)
}

func errorResponse(err error) gin.H {
	return gin.H{"error": err.Error()}
}
