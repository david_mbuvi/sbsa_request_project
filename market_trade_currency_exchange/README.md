# market_trade_currency_exchange API documentantion 

# Packages used
    github.com/gin-gonic/gin 
	github.com/golang-migrate/migrate/v4 
	github.com/golang/mock 
	github.com/lib/pq 
	github.com/spf13/viper 
	github.com/stretchr/testify 

## Description
    gin-gonic -- API Framework
    golang-migrate -- Creating database and tables
    Mock -- to mimic the actual database
    pq -- for postgres driver
    Viper -- environment management
    testify -- For testing APIs
    sqlc -- to generate golang queires and models

#Test structure
 ## api
    loaddata function
    rates test and function file
    server to hold server initilization function
## db 
    holds migration,mock,query,sqlc methods and fuction used in the test

## util 
    holds the config to load environment variables and random generator functions.

# How to Start application
    -- Docker is required on the local machine
  ## Command to run app
     docker compose up
Above command is used to build an image of the app and postgres database

# Test coverage test
    make test

# Incase of failure due to network bridge kindly run the below command to ensure that both the app and postgres runs on the same network
    make network

# sample result from my test

### Task 2 - `GET /rates/latest`
Develop an endpoint that will return the latest exchange rate. Currency should be sorted in ascending order.
![Latest Rates](Out_PutResult/LatestRates.png)

### Task 3 - `GET /rates/YYYY-MM-DD`

Develop an endpoint that will return the exchange rate on specific date. Currency should be sorted in ascending order. Response format is the same as task 2.
![RateOnSpecificDate](Out_PutResult/RateOnSpecificDate.png)

### Task 4 - `GET /rates/analyze`
Develop an endpoint that will return the minimum, maximum and average exchange rates based on the data loaded in task 1.
![Rates Analysis results](Out_PutResult/RatesAnalyze.png)