package main

import (
	"github.com/aws/aws-lambda-go/lambda"
	"log"
	"messaging_serverless_app/api"
	"messaging_serverless_app/util"
)

func main() {
	// load configuration from util folder config file
	_, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}
	lambda.Start(api.SendBirthDayGreeting())
}
