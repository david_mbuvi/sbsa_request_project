## A simple birthday notification serverless app.
I have integrated the app with plivo messaging app
 Stack scope:-
Golang and AWS Lambda, viper package for environment management

Add credentials into app.env := 
    
    ACCOUNT_SID=""
    AUTH_TOKEN=""
    PHONE_NUMBER=""


Deployed it using terraform tool
