package api

import (
	"encoding/json"
	"fmt"
	"github.com/plivo/plivo-go/v7"
	"log"
	"messaging_serverless_app/models"
	"messaging_serverless_app/util"
	"time"
)

const Filename = "../assets/BirthdayDates2022.csv"

func SendBirthDayGreeting() string {
	// today's dates
	t := time.Now()
	date := fmt.Sprintf("%02d/%02d/%02d", int(t.Month()), t.Day(), t.Year())

	// load configuration from util folder config file
	config, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	// Loop through CSV and match todays date with date in CSV

	records, err := models.ReadData(Filename)
	if err != nil {
		log.Fatal(err)
	}
	var bd models.BirthDate
	for _, record := range records {
		if record[0] == date {
			bd = models.BirthDate{
				Date:         record[0],
				Name:         record[1],
				GreetingName: record[2],
				PhoneNumber:  record[3],
			}
			break
		}
	}
	msg := "Happy Birthday " + bd.GreetingName
	accountSid := config.AccountSid
	authToken := config.AuthToken
	from := config.PhoneNumber
	to := "+254" + bd.PhoneNumber
	client, err := plivo.NewClient(accountSid, authToken, &plivo.ClientOptions{})
	if err != nil {
		fmt.Print("Error", err.Error())
	}
	response, err := client.Messages.Create(
		plivo.MessageCreateParams{
			Src:  from,
			Dst:  to,
			Text: msg,
			URL:  "", // send response status
		},
	)
	if err != nil {
		fmt.Print("Error", err.Error())
	} else {
		response, _ := json.Marshal(*response)
		fmt.Println("SMS sent successfully!")
		log.Fatal("Response: " + string(response))
	}
	return ""
}
