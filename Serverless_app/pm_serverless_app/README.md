# Golang-Serverless-App
I am Using the following  AWS tools
API Gateway + DynamoDB + Lambda
AWS Lambda :=  for storing and allowing use of App-APIs
DynamoDB := to store system records data
API Gateway :=  allows for interaction between Frontend and Backend by exposing the available APIs.

# APIs-Available
    -- CreateUser :-  used to create a new user
    --- GetUser:- Get user details using email address
    --- UpdateUser:-  Update user details
    --- DeleteUser:-  Delete users from records


# Deploy using Gitlab/Terraform

Future improvement recommended

    -- Frontend to be integrated with the endpoints
    -- User Authorization and authentication
    -- Role Based Authorization
    -- Components i.e Laptops, computers, and there status functions

Desired system :- 
  Simplified record management system  using AWS-Lambda and angular framework.

