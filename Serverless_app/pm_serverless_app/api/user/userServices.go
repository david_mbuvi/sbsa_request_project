package api

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"net/http"
	"pm_serverless_app/api"
)

var ErrorMethodNotAllowed = "method not allowed"

type ErrorBody struct {
	ErrorMsg *string `json:"error,omitempty"`
}

func GetUser(req events.APIGatewayProxyRequest, tableName string, dynaClient dynamodbiface.DynamoDBAPI) (
	*events.APIGatewayProxyResponse, error,
) {

	email := req.QueryStringParameters["email"]
	if len(email) > 0 {
		result, err := FetchUser(email, tableName, dynaClient)
		if err != nil {
			return api.ApiService(http.StatusBadRequest, ErrorBody{aws.String(err.Error())})
		}
		return api.ApiService(http.StatusOK, result)
	}

	result, err := FetchAllUsers(tableName, dynaClient)
	if err != nil {
		return api.ApiService(http.StatusBadRequest, ErrorBody{
			aws.String(err.Error()),
		})
	}
	return api.ApiService(http.StatusOK, result)

}

func CreateUser(req events.APIGatewayProxyRequest, tableName string, dynaClient dynamodbiface.DynamoDBAPI) (
	*events.APIGatewayProxyResponse, error,
) {
	result, err := CreateUserRecord(req, tableName, dynaClient)
	if err != nil {
		return api.ApiService(http.StatusBadRequest, ErrorBody{
			aws.String(err.Error()),
		})
	}
	return api.ApiService(http.StatusCreated, result)
}

func UpdateUser(req events.APIGatewayProxyRequest, tableName string, dynaClient dynamodbiface.DynamoDBAPI) (
	*events.APIGatewayProxyResponse, error,
) {
	result, err := UpdateUserRecord(req, tableName, dynaClient)
	if err != nil {
		return api.ApiService(http.StatusBadRequest, ErrorBody{
			aws.String(err.Error()),
		})
	}
	return api.ApiService(http.StatusOK, result)
}

func DeleteUser(req events.APIGatewayProxyRequest, tableName string, dynaClient dynamodbiface.DynamoDBAPI) (
	*events.APIGatewayProxyResponse, error,
) {
	err := DeleteUserRecord(req, tableName, dynaClient)

	if err != nil {
		return api.ApiService(http.StatusBadRequest, ErrorBody{
			aws.String(err.Error()),
		})
	}
	return api.ApiService(http.StatusOK, nil)
}

func UnhandledMethod() (*events.APIGatewayProxyResponse, error) {
	return api.ApiService(http.StatusMethodNotAllowed, ErrorMethodNotAllowed)
}
