package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"os"
	api "pm_serverless_app/api/user"
)

var (
	dynaClient dynamodbiface.DynamoDBAPI
)

const tableName = "simple_db"

func main() {
	region := os.Getenv("AWS_REGION")
	awsSession, err := session.NewSession(&aws.Config{
		Region: aws.String(region)})

	if err != nil {
		return
	}
	dynaClient = dynamodb.New(awsSession)
	lambda.Start(server)
}

func server(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	switch req.HTTPMethod {
	case "GET":
		return api.GetUser(req, tableName, dynaClient)
	case "POST":
		return api.CreateUser(req, tableName, dynaClient)
	case "PUT":
		return api.UpdateUser(req, tableName, dynaClient)
	case "DELETE":
		return api.DeleteUser(req, tableName, dynaClient)
	default:
		return api.UnhandledMethod()
	}
}
